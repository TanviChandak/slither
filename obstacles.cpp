#include "obstacles.h"

vector < pair <int,int> > stone;
//char stone_character = (char) 42;

pair <int, int > make_stone(){
    return {rand() % (LINES-2) + 1, rand() % (COLS-2) + 1 };
}


void init_obstacles(){
    stone.clear();
    for (int i=0; i< STONE_COUNT; i++){
        stone.push_back(make_stone());
    }   
}

void paint_obstacles(){
    for(int i=0; i<stone.size(); i++){
        move(stone[i].first, stone[i].second);
        addch('0');
    }
}

bool crash(pair <int,int> head){
    for( int i=1 ; i<stone.size(); i++){
        if(head == stone[i]){
            stone.erase(stone.begin() + i);
            stone.push_back(make_stone());
            return true;
        }
    }
    return false;
}