    #include <iostream>
    #include <curses.h>
    #include "ui.h"
    #include "game.h"
    #include "settings_constants.h"
    
    using namespace std;

    auto last_time = chrono::system_clock::now();
    
    void event_loop(){
        
        int dt;
        while(true){
            auto last_time = chrono::system_clock::now();
            erase();
            bool game_over;
            game_over = game_logic();

            if(game_over)
                break;
            
            refresh();
            do {
                auto current_time = chrono::system_clock::now();
                dt = chrono::duration_cast<std::chrono::microseconds>(current_time - last_time).count() ;
            }while(dt < TIME_DELAY_BETWEEEN_FRAMES );
            //sleep(100);
        }
    }

    int main(){
        init_ui();
        event_loop();
        tear_down_ui();
        cout << "THANKS FOR PLAYING SNAKE GAME! BUT DON\'T BE A SNAKE IN REAL WORLD " << endl;
        return 0;
    }