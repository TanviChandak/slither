#ifndef CONSTANT_H

#define CONSTANT_H

const int TIME_DELAY_BETWEEEN_FRAMES = 100000;

const int UP = 'w',
          DOWN = 's',
          RIGHT = 'd',
          LEFT = 'a';

const int ARROW_UP = 24,
          ARROW_DOWN = 25,
          ARROW_RIGHT = 26,
          ARROW_LEFT = 27;

const int BEFORE_START =0,
          STARTED = 1,
          ENDED = 2;

const int FOOD_COUNT = 20;
const int STONE_COUNT = 7;

#endif