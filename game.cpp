#include "game.h"

//int x=10 , y=10;
int game_state = BEFORE_START;
int score;

int direction ;

void start_game(){
    init_snake();
    init_food();
    init_obstacles();
    direction = UP;
    game_state = STARTED ;
    score =0;
}

void end_game(){
    game_state = ENDED ;
}

void paint_status(){
    move(0,(COLS-30));
    printw("Score %d", score);
}

bool game_logic(){
    paint_border();
    paint_status();

    int key = getch();

    if(game_state==BEFORE_START){
        move(10,10);
        addstr("Press space to begin");
        if(key==32){
            start_game();   
        }
    }
    else if (game_state == STARTED){
            if(key== UP && direction!=DOWN) 
            {
                direction = UP;
            }
            else if(key== DOWN && direction!=UP)  
            {
                direction = DOWN;
            }
            else if(key== LEFT && direction!=RIGHT)  
            {
                direction = LEFT;
            }
            else if(key==RIGHT && direction!=LEFT) 
            {
                direction = RIGHT;
            }

            pair<int,int> head = move_snake(direction);

            if(try_eating_food(head)){
                score++ ;
                grow_snake();
            }
            if(crash(head)){
                score-=2 ;
            }
            if(has_collision()){
                end_game();
            }
            paint_snake();
            paint_food();
            paint_obstacles();
        }
        else {
            // state is ended
            move(10,10);
            addstr("GAME OVER! Press spcae to restart , q to quit ");
            move (20,20);
            addstr("THANKS FOR PLAYING SNAKE GAME! BUT DON\'T BE A SNAKE IN REAL WORLD ");
            move (25, 90);
            printw("Total score: %d", score);
            if(key == 32){
                start_game();
            }
            else if(key ==  'q'){
                return true;
            }
        }
    
    return false;
}