#ifndef DEF_OBSTACLES

#define DEF_OBSTACLES

#include <curses.h>
#include <vector>
#include "settings_constants.h"
#include <random>
#include "game.h"
#include "snake.h"

using namespace std;

void init_obstacles();
pair<int,int> make_stone();
void paint_obstacles();
bool crash(pair <int,int> head);

#endif